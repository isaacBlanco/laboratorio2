
package laboratorio2;
import java.util.ArrayList;

public class personas1 {
    private int cedula;
    private String nombre;
    private String genero;
    
    public static ArrayList<personas1>listausuarios= new ArrayList<>();
    //public personas1(){//costructor
    //cedula=0;
    //String nombre="";
    //String genero="";
    
    //}
    public personas1(int cedula, String nombre, String genero){
    this.cedula=cedula;
    this.nombre=nombre;
    this.genero=genero;
    }
   
    
    

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    
}
