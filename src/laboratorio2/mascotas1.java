/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2;

import java.util.ArrayList;

/**
 *
 * @author Francisco
 */
public class mascotas1 {
    private int idmascota;
    private String nombre;
    private String tipo;
    private String estado;
    
    public static ArrayList<mascotas1>listamascotas= new ArrayList<>();
    public mascotas1(){
    int idmascota=0;
    String nombre="";
    String tipo="";
    String estado="";
    
    }
    
    public mascotas1(int idmascota, String nombre, String tipo, String estado){
        this.idmascota= idmascota;
        this.nombre=nombre;
        this.tipo=tipo;
        this.estado=estado;
    }
   

    
    public int getIdmascota() {
        return idmascota;
    }

    public void setIdmascota(int idmascota) {
        this.idmascota = idmascota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
